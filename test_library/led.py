GPIO_RGB_R = 5
GPIO_RGB_B = 11
GPIO_RGB_G =  3


def green():
    GPIO.output(GPIO_RGB_G,GPIO.HIGH)#Turn LEDs on
	time.sleep(.1)
	GPIO.output(GPIO_RGB_G,GPIO.LOW)#Turn LEDs off
	time.sleep(.1)
	
def red():
	GPIO.output(GPIO_RGB_R,GPIO.HIGH)#Turn LEDs on
	time.sleep(.1)
	GPIO.output(GPIO_RGB_R,GPIO.LOW)#Turn LEDs off
	time.sleep(.1)
	
def blue():
	GPIO.output(GPIO_RGB_B,GPIO.HIGH)#Turn LEDs on
	time.sleep(.1)
	GPIO.output(GPIO_RGB_B,GPIO.LOW)#Turn LEDs off
	time.sleep(.1)
