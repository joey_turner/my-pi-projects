import os

def file_rename():
    os.chdir("/usr/share/adafruit/webide/repositories/my-pi-projects/Programming-Foundations-with-Python/prank/prank")
    file_list = os.listdir("/usr/share/adafruit/webide/repositories/my-pi-projects/Programming-Foundations-with-Python/prank/prank")
    print(file_list)

    for file_name in file_list:
        os.rename(file_name, file_name.translate(None, "0123456789"))

file_rename()
