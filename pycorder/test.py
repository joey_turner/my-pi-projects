#!/usr/bin/python
#+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#|R|a|s|p|b|e|r|r|y|P|i|-|S|p|y|.|c|o|.|u|k|
#+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#
# ultrasonic_1.py
# Measure distance using an ultrasonic module
#
# Author : Matt Hawkins
# Date   : 09/01/2013
# -----------------------

# Import required Python libraries
import time
import RPi.GPIO as GPIO

# Use BCM GPIO references
# instead of physical pin numbers
GPIO.setmode(GPIO.BCM)

# Define GPIO to use on Pi

GPIO_TRIGGER = 23
GPIO_ECHO    = 24

print "Ultrasonic Measurement"

# Set pins as output and input
GPIO.setup(GPIO_TRIGGER,GPIO.OUT)  # Trigger
GPIO.setup(GPIO_ECHO,GPIO.IN)      # Echo

def echo():
    GPIO.output(GPIO_TRIGGER, False)	# Set trigger to False (Low)
    time.sleep(0.5)						# Allow module to settle
    GPIO.output(GPIO_TRIGGER, True)		# Send 10us pulse to trigger
    time.sleep(0.00001)
    GPIO.output(GPIO_TRIGGER, False)
    while GPIO.input(GPIO_ECHO)==0: #start = time.time()
        start = time.time()

    while GPIO.input(GPIO_ECHO)==1:
        stop = time.time()
    elapsed = stop-start			# Calculate pulse length
    distance = elapsed * 34300		# Distance pulse travelled in that time is time multiplied by the speed of sound (cm/s)
    distance = distance / 2			# That was the distance there and back so halve the value
	#print "Echo Location"
	#print "Distance : %.1fcm" % distance
    return distance
	