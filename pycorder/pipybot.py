#!/usr/bin/python
import time
import os
import RPi.GPIO as GPIO
from Adafruit_CharLCD import Adafruit_CharLCD
from time import sleep
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(3,GPIO.OUT)
GPIO.setup(5,GPIO.OUT)
GPIO.setup(7,GPIO.OUT)
GPIO.setup(11,GPIO.OUT)
GPIO_RGB_R = 5
GPIO_RGB_B = 11
GPIO_RGB_G =  3
GPIO_TRIGGER = 20
GPIO_ECHO    = 21
GPIO.setup(GPIO_TRIGGER,GPIO.OUT)  	# Trigger
GPIO.setup(GPIO_ECHO,GPIO.IN)      	# Echo
loop_count = 0


def green():
	GPIO.output(GPIO_RGB_G,GPIO.HIGH)#Turn LEDs on
	time.sleep(.1)
	GPIO.output(GPIO_RGB_G,GPIO.LOW)#Turn LEDs off
	time.sleep(.1)
	
def red():
	GPIO.output(GPIO_RGB_R,GPIO.HIGH)#Turn LEDs on
	time.sleep(.1)
	GPIO.output(GPIO_RGB_R,GPIO.LOW)#Turn LEDs off
	time.sleep(.1)
	
def blue():
	GPIO.output(GPIO_RGB_B,GPIO.HIGH)#Turn LEDs on
	time.sleep(.1)
	GPIO.output(GPIO_RGB_B,GPIO.LOW)#Turn LEDs off
	time.sleep(.1)
	

def echo():
	GPIO.output(GPIO_TRIGGER, False)	# Set trigger to False (Low)
	time.sleep(0.5)						# Allow module to settle
	GPIO.output(GPIO_TRIGGER, True)		# Send 10us pulse to trigger
	time.sleep(0.00001)
	GPIO.output(GPIO_TRIGGER, False)

	while GPIO.input(GPIO_ECHO)==0: #start = time.time()
		start = time.time()

	while GPIO.input(GPIO_ECHO)==1:
		stop = time.time()
	elapsed = stop-start			# Calculate pulse length
	distance = elapsed * 34300		# Distance pulse travelled in that time is time multiplied by the speed of sound (cm/s)
	distance = distance / 2			# That was the distance there and back so halve the value
	#print "Echo Location"
	#print "Distance : %.1fcm" % distance
	return distance
	
loop_count = input("How many times would you like loop?: ")

while loop_count > 0:
    distance = str(round(echo()))
    loop_count = loop_count - 1
    time.sleep(0.5)      
    if __name__ == '__main__':
  
        lcd = Adafruit_CharLCD()
        lcd.clear()
        lcd.message(distance + ' cm\n'+str(loop_count))
    else:

        print distance + '\n' + 'count'+str(loop_count)
        time.sleep(.5)
    
    
    
 